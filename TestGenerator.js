/*
 * Copyright (c) 2016.11.2
 * Created by W3CProject
 */

"use strict";

function TestGenerator(selector) {
    this._mainContainer = document.getElementById(selector);
    this._send = document.getElementById('send');
    this._send.value = 'Send';
}

TestGenerator.prototype.TestName = function (name) {
    var testTitle = name;
    var testNameContainer = document.createElement('span');

    testNameContainer.className = 'test-title';
    testNameContainer.innerHTML = testTitle;

    this._mainContainer.appendChild(testNameContainer);
    return this;
};

TestGenerator.prototype.QuestionTitle = function (title) {
    var questionNameContainer = document.createElement('span');

    questionNameContainer.className = 'question-title';
    questionNameContainer.innerHTML = title;

    this._mainContainer.appendChild(questionNameContainer);

    return this;
};

TestGenerator.prototype.QuestionList = function (options) {
    var questionULClass = options.ulClass || 'default-test';
    var questionULID = options.ulID || '';
    var questionLabelName = options.labelName || '';
    var questionValueItems = options.inputValue || ' ';
    var questionNameItems = options.inputName || 'defaultName' + getRandomID(0, 100);
    var listLength = questionLabelName.length;
    var questionULContainer = document.createElement('ul');

    questionULContainer.className = questionULClass;
    questionULContainer.id = questionULID;

    this._mainContainer.appendChild(questionULContainer);

    for (var j = 0; j < listLength; j++) {
        var singleQuestion = document.createElement('li');
        var input = document.createElement('input');
        var label = document.createElement('label');
        var randID = questionNameItems + getRandomID(1, 100);

        input.type = 'radio';
        input.value = questionValueItems[j];
        input.name = questionNameItems;
        input.id = randID;

        label.setAttribute('for', randID);
        label.innerHTML = questionLabelName[j];

        questionULContainer.appendChild(singleQuestion);
        singleQuestion.appendChild(input);
        singleQuestion.appendChild(label);
    }
    return this;
};

TestGenerator.prototype.QuestionEvents = function (events) {
    var event = events.event || 'click';
    var element = events.element || this._send;

    Event.observe(element, event, function (send) {
       console.log(element, event);
    });
};

function getRandomID(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

window.onload = function () {
    var testGenerator = new TestGenerator('test1');

    testGenerator.TestName('MyTest')
        .QuestionTitle('FirstQuestionTitle')
        .QuestionList({
            labelName: [
                'first',
                'second',
                'third'
            ],
            inputValue: [
                '1',
                '2',
                '3'
            ]
        })
        .QuestionEvents({
            event: 'click'
        });
};