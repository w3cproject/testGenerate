/*
 * Copyright (c) 2016.11.10
 * Created by W3CProject
 */

"use strict";

var default_values = {
    'mainContainer': 'testBody',
    'submitID': 'send_test_sd',
    'submitValue': 'Получить результат',
    'inputValuesResultID': 'sum_result'
};

window.TestSdGenerator = function TestSdGenerator(id) {
    //vars
    this._mainContainer = document.getElementById(default_values['mainContainer'] || id);
};

TestSdGenerator.prototype.CreateSingleItem = function (options) {
    //options
    this._listContainerID = options.ContainerID;
    this._questionTitle = options.Title || 'default-title';
    this._questionCaption = options.Caption;

    //vars
    var mainContainer = this._mainContainer;
    var questionItem = document.createElement('div');
    var questionTitle = document.createElement('div');
    this._questionListContainer = document.createElement('ul');
    var questionListContainer = this._questionListContainer;
    var questionCaption = document.createElement('div');

    //attributes
    questionItem.className = 'question_item';
    questionTitle.className = 'question';
    questionListContainer.className = 'answer';
    questionListContainer.setAttribute('id', this._listContainerID);
    questionCaption.className = 'caption';

    //appends
    mainContainer.appendChild(questionItem);
    questionItem.appendChild(questionTitle);
    questionItem.appendChild(questionCaption);
    questionTitle.innerHTML = this._questionTitle;
    questionItem.appendChild(questionListContainer);

    return this;
};

TestSdGenerator.prototype.ShowQuestionList = function (options) {
    //vars
    this._inputID = options.inputID;
    this._inputType = options.inputType || 'radio';
    this._inputName = options.inputName;
    this._inputValue = options.inputValue || '';
    this._labelName = options.labelName || 'default-label';
    var questionListContainer = this._questionListContainer;
    var questionListItem = document.createElement('li');
    var questionInput = document.createElement('input');
    var questionLabel = document.createElement('label');

    questionLabel.setAttribute('for', this._inputID);
    questionInput.setAttribute('id', this._inputID);
    questionInput.setAttribute('type', this._inputType);
    questionInput.setAttribute('name', this._inputName);
    questionInput.value = this._inputValue;

    questionListContainer.appendChild(questionListItem);
    questionListItem.appendChild(questionInput);
    questionListItem.appendChild(questionLabel);
    questionLabel.innerHTML = this._labelName;

    return this;
};

window.onload = function () {
    var testSdGenerator = new TestSdGenerator();

    testSdGenerator
        .CreateSingleItem({
            questionTitle: '1. Возраст',
            listContainerID: 'old'
        })
        .ShowQuestionList({
            inputID: 'old1',
            inputType: 'radio',
            inputName: 'old',
            inputValue: '0',
            labelName: 'До 45 лет'
        })
        .ShowQuestionList({
            inputID: 'old2',
            inputType: 'radio',
            inputName: 'old',
            inputValue: '2',
            labelName: 'До 50 лет'
        })
        .CreateSingleItem({
            questionTitle: '2. fwerg',
            listContainerID: 'old2'
        })
        .ShowQuestionList({
            inputID: 'old2',
            inputType: 'radio',
            inputName: 'old2',
            inputValue: '4',
            labelName: 'До 45 лет'
        });
};